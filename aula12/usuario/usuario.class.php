<?php 

class Usuario {
    private $id;
    private $nome;
    private $email;
    private $senha;
    private $user;
    private $objDb;

    public function __construct(){
        $this->objDb =  new mysqli('127.0.0.1', 'root', '', 'aula_php', 3307);
        echo "Vai ser feito a conexao com o banco";
    }
 
    public function setId(int $id){
        $this->id = $id;
    }

    public function setNome(string $nome){
        $this->nome = $nome;
    }

    public function setEmail(string $email){
        $this->email = $email;
    }

    public function setSenha(string $senha){
        $this->senha = $senha;
    }

    public function getId():int{
        return $this->id;
    }

    public function getNome():string{
        return $this->nome;
    }

    public function getEmail():string{
        return $this->senha;
    }

    public function getSenha():string{
        return $this->senha = password_hash( $senha, PASSWORD_DEFAULT);
    }

    public function saveUser(){

        $objStmt = $this->objDb->prepare('REPLACE INTO login(id, Nome, Email, senha) VALUES(?,?,?,?)');
        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);
        return $objStmt->execute();
    }

    public function deleteUser(){

        $objStmt = $this->objDb->prepare('DELETE FROM login WHERE id = ?');
        $objStmt->bind_param('i', $this->id);
        return $objStmt->execute();
    }

    public function viewUser(int $id_consulta){

        $query = "SELECT id, Nome, Email, senha FROM login";

        if ($result = $this->objDb->query($query)) {
            while ($row = $result->fetch_row()) {
                printf("%s (%s,%s)\n", $row[0], $row[1], $row[2]);
            }
            /* free result set */
            $result->close();
        }
        
    }
    

    public function __destruct(){
        unset($this->objDb);
        echo "Vai ser finalizado a conexao com o banco";
    }

}