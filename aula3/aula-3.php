<?php

echo "<pre>";

//CONSTANTE NO PHP
define('qtd_paginas', 10);
echo "Valor da minha constante é:".qtd_paginas. "<br>";


//VARIAVEL PARA PASSAR VALOR PARA UMA CONSTANTE
$ip_do_banco = '192.168.45.12';
define('ip_do_banco', $ip_do_banco);

echo "O ip do banco é ".$ip_do_banco;


echo "\nEstou na linha ".__LINE__;

echo "\nEste é o arquivo ".__FILE__;


//DEPURAÇAO DE CODIGO

echo "\n\n";
var_dump("teste - ".$ip_do_banco);

/* VETOR */
echo "\n\n";
unset($dias_semana);

$dias_semana = 
[
    'dom',
    'seg',
    'ter',
    'qua',
    'qui',
    'sex',
    'sab'
];

$dias_semana[0] = 'dom';
$dias_semana[1] = 'seg';
$dias_semana[2] = 'ter';
$dias_semana[3] = 'qua';
$dias_semana[4] = 'qui';
$dias_semana[5] = 'sex';
$dias_semana[7] = 'sab';

var_dump($dias_semana);

echo "\n\n";




echo "</pre>";
?>


