<?php
session_start();

//Verifica se o usário pode acessar a funcionalidade
if( !isset($_SESSION['id_usuario']) ){

	header('Location: ../login.php');

	exit();
}

echo '<a href="../menu.php">Voltar</a><br><br>';
echo 'Aqui ser&atilde;o listados os usu&aacute;rios<br>';
?>

<?php 

    // Conectar ao banco

    if($db = mysqli_connect('127.0.0.1', 'root', '', 'aula_php', 3307)){
        //echo "Conectado!";
    }else{
        die("Ocorreu um erro");
    }

    $dados['nome'] = null;

	$dados['senha'] = null;
	
	$dados['email'] = null;

    $id = null;

    
  // Formulário

  ?>

  <!-- Se clicar em editar recupera o cadastro do sistema -->
  <?php
      if(isset($_POST['editar'])){
          $id = preg_replace('/\D/', '', $_POST['editar']);
          $prep = mysqli_prepare($db,'SELECT nome, url FROM login WHERE id = ?');
          mysqli_stmt_bind_param($prep, 'i', $id);

          mysqli_stmt_execute($prep);
          $result = mysqli_stmt_get_result($prep);

          $dados = $result->fetch_assoc();
      }
  
  ?>


<div class="formulario">
  <h3>Formulario principal</h3>

  <form method='post'>
      <label for="input-id">Id:</label>
      <input name="id" type="hidden" id="input-id" value=<?php echo $id; ?>><br>

      <label for="input-nome">Nome:</label>
      <input name="nome" type="text" id="input-nome" value=<?php echo $dados['nome']; ?>><br>

      <label for="input-bit">E-mail:</label>
      <input type="text" name="email" id="input-bit" value=<?php echo $dados['email']; ?>><br>

        <label for="input-bit">Senha:</label>
        <input type="text" name="senha" id="input-bit" value=<?php echo $dados['senha']; ?>><br>
      
      <input type="submit" name="enviar">
  </form>
</div>


  <?php
  

  
  //Acessando as variaveis
  $id = isset(($registro['id']));
  $nome = isset($_POST['nome']) ? $_POST['nome'] : null;
  $email = isset($_POST['email']) ? $_POST['email'] : null;
  $senha = isset($_POST['senha']) ? $_POST['senha'] : null;



  // Consulta no banco
  if( empty($registro['id']) ){
    //   $query_preparada = mysqli_prepare($db,'INSERT INTO login(nome, email, senha) VALUES (?,?,?)');

    //   mysqli_stmt_bind_param($query_preparada, 'sss', $nome, $email, $senha);

    //   if(mysqli_stmt_execute($query_preparada)){
    //       echo "<br><br> Dados de $nome gravados!";
    //   }
      $objquery = mysqli_query($db, 'SELECT id,nome,email,senha FROM login');
  
  }elseif( is_numeric($_POST['id']) ){
      $p = mysqli_prepare( $db, '	UPDATE login SET	nome = ?, email = ?, senha = ? WHERE	id = ?');

      mysqli_stmt_bind_param(	$p, 
          'sssi', 
          $nome, 
          $email, 
          $senha,
          $registro['id']);

      if( mysqli_stmt_execute($p)){

          echo "<br><br>Dados de $nome atualizados no SGDB!";
      }
  }

  echo "<pre>\n";
  ?>
  <form method="post">
  <table>
          <tr>
              <th>Id</th>
              <th>Nome</th>
              <th>E-mail</th>
              <th>Senha</th>
          </tr>
      <?php

      while($registro = $objquery->fetch_assoc()):
          //var_dump($registro);
      ?>

          <tr>
              <td>
                  <?php echo $registro['id'];?>
              </td>
              <td>
                  <?php echo $registro['nome'];?>
              </td>
              <td>
                  <?php echo $registro['email']; ?>
              </td>
              <td>
                  <?php echo $registro['senha'];?>
              </td>
          </tr>

      <?php  endwhile; ?>
  </table>
  </form>
<style>
table {
font-family: arial, sans-serif;
border-collapse: collapse;
width: 100%;
}

td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
}

tr:hover {
background-color: #dddddd;
}

form {
  padding: 10px 30px 10px 30px;
}

.formulario {
  background: #f1f1f1;
  width: 500px;
  border-radius: 4px;
  padding:10px 20px 0 10px;
  margin: 50px auto;
}

.formulario h3 {
  text-align: center;
  font-size: 20px;
  font-family: monospace;
}

.formulario input[type='text'] {
  margin: 0 auto;
  display: block;
  width:100%;
  height:35px;
}

.formulario label {
  text-align: center;
}

input[type="submit"] {
  width: 200px;
  height: 40px;
  border-radius: 4px;
  border: none;
  background: #6c5ce7;
  color: #fff;
  text-transform: uppercase;
  font-weight: 800;
  margin-bottom: 30px;
  cursor:pointer;
}

.btn-editar {
  border: none;
  background: #a29bfe;
  border-radius: 4px;
  height: 30px;
  width: 100%;
  color: #fff;
  font-weight: 800;
  text-transform: uppercase;
  cursor:pointer;
}

.btn-apagar {
  border: none;
  background:#e17055;
  border-radius: 4px;
  height: 30px;
  width: 100%;
  color: #fff;
  font-weight: 800;
  text-transform: uppercase;
  cursor:pointer;
}

</style>