<?php
//Conecta no banco
if( $db = mysqli_connect(	'localhost',
							'root',
							'',
							'aula_php',
							3307) ){

} else {

	die("Problema ao conectar ao SGDB");
}

$login = null;

if( isset($_POST['email']) ){
	$p = mysqli_prepare( $db, '	SELECT
									id, nome, senha
								FROM
									login
								WHERE
									email = ?');
		mysqli_stmt_bind_param(	$p, 
								's', 
								$_POST['email']);
		mysqli_stmt_execute($p);
		$result = mysqli_stmt_get_result($p);
		$existe = $result->num_rows;
		$usuario = $result->fetch_assoc(); 

		if( $existe > 0 &&
			$_POST['password'] == $usuario['senha']){

			$login = true;

		}else{

			$login = false;
		}
}

//Se o login ocorreu de forma satisfatória
if($login === true){

	session_start();

	$_SESSION['id_usuario'] = $usuario['id'];

	$_SESSION['nome_usuario'] = $usuario['nome'];

	require('menu.php');

	exit();
}

//Credenciais com erro 
if($login === false){

	echo '<br><br>Login ou senha inválidos<br><br>';
}

echo '	<form method="POST">
				E-mail: <input 
								type="text" 
								name="email"><br>
				Senha: <input 
								type="password"
								name="password">
				<input type="submit" value="Entrar">
		</form>';